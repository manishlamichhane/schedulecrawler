<?php 
	
	include "simple_html_dom.php";
	
	class NextClass{

		private $website;
		private $today;
		private $parentNode;

		function __construct(){

			$this->website 	=	file_get_html("https://se.timeedit.net/web/ltu/db1/schedule1/ri177813X15Z0QQ5Z36g9620y00Y6Y78307gQY5Q578.html");
			$this->today	=	date("j/n/Y");
			$this->parentNode	=	$this->getSchedule();
		}

		function getSchedule(){

			$children	=	$this->website->find("td");
			$dateExploded =	explode("/",$this->today); //to get the day and month parameter from the date
			$date = $dateExploded[0]."/".$dateExploded[1]; //creating a new variable with just day/month format as that is what is used in the website from where the schedule is drawn
			foreach($children as $td){
				if($td->innertext == $date)
					return $td->parent(); 
			}
			
			/* 
				*if the function control reaches here without breaking from the loop 
				*it means that the date returned by date("j/n") parameter is not listed in the DOM
				*it means that this script is run in days other than the ones that have schedule actually listed in the website
				*In that case, we will keep on increasing the date by each day until we actually reach a date value that is listed in the DOM

-			*/

			$this->today = str_replace("/", "-", $this->today);
			$this->today = date("j/n/Y",strtotime($this->today."+1 day"));
			return $this->getSchedule($this->today);
		}

		function getParent(){

			return $this->parentNode;
		}

		function formatData(){

			$data = str_get_html($this->parentNode);
			echo "Next class on: ";
			echo "Day: ".$data->find("td",1)->innertext."\n";
			echo "Date: ".$data->find("td",2)->innertext."\n";
			echo "Time: ".$data->find("td",3)->innertext."\n";
			echo "Room no: ".$data->find("td",4)->innertext."\n";
			echo "Lecture by: ".$data->find("td",5)->innertext."\n";
			echo "Type: ".$data->find("td",6)->innertext."\n";
			echo "Course: ".$data->find("td",7)->innertext."\n";
			echo "Location: ".$data->find("td",8)->innertext."\n";
		}

	}

	$scheduler 	=	new NextClass();
	$scheduler->formatData();
	

?>