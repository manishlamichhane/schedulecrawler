# README #

This application is a collection of PHP and Shell scripts that will crawl LTU's schedule website and pull the schedule of Assoc.Prof. Karan Mitra's courses schedule.
The shell script is to be run as a CRON job so that it will automatically fetch the schedule, read it out loud and also display the notification.
The script has been tested in Ubuntu and LinuxMint Rosa only.

It uses espeak application in linux to read the schedule out loud.

